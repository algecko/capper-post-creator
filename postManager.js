const validUrl = require('valid-url')
const postDal = require('./postDal')
const {createId, parseId, validateReqField} = require('./postTools')
const {countWords} = require('./tools')

const prepareTags = (post) => {
	let tags = []
	if (post.tags) {
		tags = post.tags.split(/[,;]/g)
		tags.map(tag => tag.replace(/([^a-zA-Z0-9\-_,])/g, '').trim())
	}

	return tags
}

async function validateUser(post, user) {
	if (!post.id) {
		post.author = user
		return post
	} else {
		post.id = parseId(post.id)
		const storedPost = await postDal.findPost(post.id)
		if (!storedPost)
			throw {statusCode: 404, extMessage: 'Post to be edited does not exist'}
		if (storedPost.author.toLowerCase() !== user.toLowerCase())
			throw {statusCode: 401, extMessage: 'Only your own posts can be edited'}

		return {...storedPost, ...post, date: storedPost.date}
	}
}

async function prepareAndValidate(passedPost, user) {
	['text', 'title'].forEach(field => validateReqField(passedPost, field))
	const post = await validateUser(passedPost, user)

	if (!post.noImg && !validUrl.isWebUri(post.imgUrl))
		throw {extMessage: 'Image url must be a valid url'}

	if (post.noImg) {
		const plainUrls = post.text.match(/!\[.*]\(.+\)/g)
		if (!plainUrls)
			throw {extMessage: 'Please include at least one image in your post'}
		const imgUrls = plainUrls.map(item => item.substring(item.indexOf(']') + 2, item.length - 1))

		const invalidUrls = imgUrls.filter(url => !validUrl.isWebUri(url))
		if (invalidUrls.length > 0)
			throw {extMessage: `Some of the image urls appear to be invalid "${invalidUrls.join('", "')}"`}
	}

	if (post.noImg)
		delete post.imgUrl
	if (post.author && post.author !== user)
		throw {statusCode: 401, extMessage: 'Logged in user is not hte posts author'}
	post.date = post.id ? (post.date || new Date()) : new Date()
	post.id = post.id || createId(post)
	post.title = post.title.trim()
	post.text = post.text.replace(/\r{4,}/g, '\r\r\r').trim()
	post.tags = prepareTags(post)
	post.size = countWords(post.text)
	return post
}

module.exports = {prepareAndValidate}