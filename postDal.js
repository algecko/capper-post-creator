const db = require('./db')
const getCollection = () => db.get().collection('posts')

module.exports.savePost = function (post) {
	return getCollection().update({
		id: post.id
	}, post, {upsert: true})
		.catch(() => {
			throw {extMessage: 'Error saving Post'}
		})
}

module.exports.findPost = (id) => getCollection()
	.findOne({id})
