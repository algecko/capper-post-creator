module.exports.countWords = (text) =>
	text
		.replace(/[\s"!?.:+*#`˚';\-\/\\\r\n]+/g, ' ')
		.trim()
		.split(' ')
		.length