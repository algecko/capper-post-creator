const crypto = require('crypto')
const atob = require('atob')

const hash = (items) => {
	const hash = crypto.createHash('md5')
	items.forEach(item => {
		hash.update(item)
	})
	return hash.digest('hex')
}

const isBase64 = (text) => !!text.match(/^([A-Za-z0-9+/]{4})*([A-Za-z0-9+/]{4}|[A-Za-z0-9+/]{3}=|[A-Za-z0-9+/]{2}==)$/)

module.exports.createId = (post) => hash([post.author, post.title, post.date.toString()])

module.exports.parseId = (id) => !id || (typeof id !== 'string') || id.length === 0 ? undefined :
	isBase64(id) && atob(id).toLowerCase().startsWith('post:') ? atob(id).substring(5)
		: id


module.exports.validateReqField = (object, fieldName) => {
	if (!object[fieldName] || object[fieldName].length === 0)
		throw {extMessage: `${fieldName} is required`}
}